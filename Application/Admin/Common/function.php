<?php
// * @Author: defaultFish
// * @Date:   2018-01-01 00:00:00
// * +
// * | siteCMS [ siteCMS网站内容管理系统 ]
// * | Copyright (c) 2018 http://www.sitecms.cn All rights reserved.
// * | Licensed ( http://www.sitecms.cn/licenses/ )
// * | Author: defaultfish <defaultfish@qq.com>
// * +

//获取系统配置信息
function getSysInfo(){
    $data = S('sysinfo') ? S('sysinfo') : M('SysConfig')->where(array('id'=>1))->find();
    return $data;
}
// 格式化数组(多维数组)
function formatTreeArr($array,$pid = 0){
    $tree = array(); //每次都声明一个新数组用来放子元素
    foreach($array as $v){
        if($v['pid'] == $pid){ //匹配子记录
            $v['children'] = formatTreeArr($array,$v['id']); //递归获取子记录
            $tree[] = $v;//将记录存入新数组
        }
    }
    return $tree;//返回新数组
}
// 格式化数组(一维数组)
function toLevel($cate,$delimiter = '&nbsp;&nbsp;&nbsp;&nbsp;',$parent_id = 0,$level = 0){
    $arr = array();
    foreach($cate as $v){
        if($v['pid'] == $parent_id){
            $v['children'] = hasChild($cate,$v['id']) ? 1 : 0;
            $v['level'] = $level + 1;
            $v['delimiter'] = str_repeat($delimiter,$level);
            $arr[] = $v;
            $arr = array_merge($arr,toLevel($cate,$delimiter,$v['id'],$v['level']));
        }
    }
    return $arr;
}
// 传递一个父级分类ID返回所有子级分类
function getChilds($cate,$pid){
    $arr = array();
    foreach($cate as $v){
        if($v['pid'] == $pid){
            $arr[] = $v;
            $arr = array_merge($arr,getChilds($cate,$v['id']));
        }
    }
    return $arr;
}
// 判断分类是否有子分类,返回false,true
function hasChild($cate,$id){
    $arr = false;
    foreach($cate as $v){
        if($v['pid'] == $id){
            $arr = true;
            return $arr;
        }
    }
    return $arr;
}
// 传递一个父级分类ID返回所有子分类ID
function getChildsId($cate,$pid,$flag = 0){
    $arr = array();
    if($flag){
        $arr[] = $pid;
    }
    foreach($cate as $v){
        if($v['pid'] == $pid){
            $arr[] = $v['id'];
            $arr = array_merge($arr ,getChildsId($cate,$v['id']));
        }
    }
    return $arr;
}
// 传递一个分类ID返回该分类相当信息
function getSelf($cate,$id){
    $arr = array();
    foreach($cate as $v){
        if($v['id'] == $id){
            $arr = $v;
            return $arr;
        }
    }
    return $arr;
}
//Uploadify上传图片
function uploadify(){
    if(!empty($_FILES)){
        //上传参数配置
        $config=array(
            'maxSize'    =>    10240000,// 设置附件上传大小
            'rootPath'   =>    'Public/uploads/images/',// 设置附件上传根目录
            'savePath'   =>    '',// 设置附件上传（子）目录
            'saveName'   =>    date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8),//保存名称
            'exts'       =>    array('jpg', 'gif', 'png', 'jpeg'),// 设置附件上传类型
            'autoSub'    =>    true,//自动使用子目录保存上传文件 默认为true
            'subName'    =>    array('date','Ymd'),//子目录创建方式，采用数组或者字符串方式定义
        );
        $upload = new \Think\Upload($config);// 实例化上传类
        // 上传文件
        $info = $upload -> upload();
        if(!$info){
            // 上传错误提示错误信息
            $data = array(
                'status'	=> 0,//状态
                'info'		=> $upload -> getError(),//错误信息
            );
        }else{
            $data = array(
                'status'	=> 1,
                'info'		=> '图片上传成功！',
                'image_src'	=> '/'.$config['rootPath'].$info['Filedata']['savepath'].$info['Filedata']['savename'],
            );
        }
        return $data;
    }
}
//删除图册图片
function galleryDelete($gid){
    $listPhoto = M('PageGallery') -> where(array('gid' => $gid)) -> select();
    foreach($listPhoto as $value){
        $img_url = '.'.$value['image_src'];
        @unlink($img_url);
    }
    $resultPhoto = M('PageGallery') -> where(array('gid' => $gid)) -> delete();
}
//清除缓存
function rmdirr($dirname) {
    if (!file_exists($dirname)) {
        return false;
    }
    if (is_file($dirname) || is_link($dirname)) {
        return unlink($dirname);
    }
    $dir = dir($dirname);
    if($dir){
        while (false !== $entry = $dir->read()) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            //递归
            rmdirr($dirname . DIRECTORY_SEPARATOR . $entry);
        }
    }
    $dir->close();
    return rmdir($dirname);
}
//数据库备份
function format_bytes($size, $delimiter = '') {
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $size >= 1024 && $i < 5; $i++) $size /= 1024;
    return round($size, 2) . $delimiter . $units[$i];
}
