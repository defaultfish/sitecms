<?php
namespace Admin\Controller;
class AdminController extends BasicController {
    // 定义全局参数
    private $table = 'SysAdmin';

    function _initialize(){
        parent::_getNode();
        //管理员
        $adminInfo = M($this->table)->where(array('id'=>session('admin_id')))->find();
        $this->assign('adminInfo',$adminInfo);
        //得到节点内容
        $this->nodeInfo = M('SysNode')->where(array('id'=>$this->nid))->find();
        $this->assign('nodeInfo', $this->nodeInfo);
    }
    public function index(){
        $where['id'] = array('NEQ',1);
        $list = M($this->table)->where($where)->select();
        $this->assign('list', $list);

        $this->display();
    }
    // 新增
    public function insert(){
        $this->display('form');
    }
    public function add(){
        if(IS_AJAX){
            $Model_Data = M($this->table);
            if($Model_Data->create()){
                $username = trim(I('username'));
                $password = trim(I('password'));
                $user = $Model_Data->where(array('username'=>$username))->find();
                !empty($user) && $this->error('登录名已存在，请重新输入!');
                $Model_Data->password = md5($password.C('salt'));
                $Model_Data->add() !== false ? $this->success('新增成功!',U('index').'?spm='.$this->spm) : $this->error('新增失败!');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            echo "非法操作";
        }
    }
    // 编辑
    public function update(){
        $view = M($this->table)->where(array('id'=>I('get.id')))->find();
        $this->assign('view', $view);

        $this->display('form');
    }
    public function save(){
        if(IS_AJAX){
            $Model_Data = M($this->table);
            if($Model_Data->create()){
                $password = trim(I('password'));
                if ($password) {
                    $Model_Data->password = md5($password.C('salt'));
                } else {
                    unset($Model_Data->password);
                }
                $Model_Data->save() !== false ? $this->success('保存成功!',U('index').'?spm='.$this->spm) : $this->error('保存失败!');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            echo "非法操作";
        }
    }
    // 删除
    public function delete(){
        if(IS_AJAX){
            $where['id'] = array('IN',I('get.id'));
            M($this->table)->where($where)->delete() !== false ? $this->success('删除成功!',U('index').'?spm='.$this->spm) : $this->error('删除失败!');
        }else{
            echo "非法操作";
        }
    }
    // 修改状态
    public function status(){
        if(IS_AJAX){
            $new_status = I('value')?'0':'1';
            M($this->table)->where(array('id'=>I('id')))->setField('is_status',$new_status) !== false ? $this->success('修改成功!','',$new_status) : $this->error('修改失败!');
        }else{
            echo "非法操作";
        }
    }
    // 重置密码
    public function pass(){
        $this->display('pass');
    }
    public function resetpass(){
        if (IS_AJAX) {
            $Model_Data = M($this->table);
            if($Model_Data->create()){
                $old_password = trim(I('old_password'));
                $password = trim(I('password'));
                $user = $Model_Data->where(array('id'=>session('admin_id')))->find();
                $user['password'] !== md5($old_password.C('salt')) && $this->error('原密码错误，请重新输入!');
                $Model_Data->where(array('id'=>session('admin_id')))->setField('password',md5($password.C('salt'))) !== false ? $this->success('修改成功!','') : $this->error('修改失败，新密码不能与旧密码一致!','');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            return '非法操作';
        }
    }
    //修改信息
    public function info(){
        $this->display('info');
    }
    public function resetinfo(){
        if (IS_AJAX) {
            $Model_Data = M($this->table);
            if($Model_Data->create()){
                $Model_Data->where(array('id'=>session('admin_id')))->save() !== false ? $this->success('修改成功!','') : $this->error('修改失败!');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            return '非法操作';
        }
    }
    //清除缓存
    public function cache_clear() {
        $dirs = array(APP_PATH.'/Runtime/');
        //清理缓存
        foreach($dirs as $value){
            rmdirr($value);
        }
        $this->success('系统缓存清除成功！');
    }
}
