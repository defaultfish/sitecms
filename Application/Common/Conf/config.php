<?php
return array(
    //数据库配置信息
	'DB_TYPE'    => 'mysql', // 数据库类型
	'DB_HOST'    => 'bdm248477394.my3w.com', // 服务器地址
	'DB_NAME'    => 'bdm248477394_db', // 数据库名
	'DB_USER'    => 'bdm248477394', // 用户名
	'DB_PWD'     => '2YHrYzav', // 密码
	'DB_PREFIX'  => 'sc_', // 数据库表前缀
	'DB_CHARSET' => 'utf8', // 字符集
	'DB_DEBUG'   => false, // 数据库调试模式 开启后可以记录SQL日志 3.2.3新增
    // 开启路由
    'URL_ROUTER_ON'   => true,
    // URL模式
    'URL_MODEL' => 2,
    // 开启子域名配置
    'APP_SUB_DOMAIN_DEPLOY'   =>    1,
    'APP_SUB_DOMAIN_RULES'    =>    array(
        'admin' => 'Admin',
    ),
    //简化模板的目录层次
    'TMPL_FILE_DEPR'=>'_',
    'TAGLIB_BEGIN'          =>  '{',            // 模板引擎普通标签开始标记
    'TAGLIB_END'          =>  '}',            // 模板引擎普通标签结束标记
    //全局加密盐巴
	'salt' => 'sitecms.cn',
	//模板路径配置
    'TMPL_PARSE_STRING' => array(
        '__Plugs__'     => '/Public/plugs' ,
        '__Base__'      => '/Public/static/base' ,
        '__Admin__'     => '/Public/static/admin' ,
		'__Default__'   => '/Public/static/index' ,
    ),
);
